{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [
     pkgs.rustup
     pkgs.cargo

     # for dev help
     pkgs.cargo-watch
     pkgs.cargo-insta
  ];
  # Set Environment Variables
  RUST_BACKTRACE = 1;
}

