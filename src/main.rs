use std::process::Command;
use log::{debug, trace};

// for now im being a little lazy every error is a panic
// in the future maybe we can make some warnings within the for loop but tbh the program is sooo tiny
// there might be little point
fn main() {
    env_logger::init();
    trace!("Starting left hand mouse remap");
    let xinput_output = Command::new("xinput")
        .output()
        .expect("failed to execute process");

    debug!("Successfully executed xinput to get devices, handling output");

    let mut split_keyboard_mouse = std::str::from_utf8(&xinput_output.stdout)
        .expect("is string")
        .split("Virtual core keyboard");
    trace!("split output to get 1 set for keyboard and one set for mice");
    let mouse_newline = split_keyboard_mouse
        .next()
        .expect("Sting")
        .split('\n')
        .skip(1);

    trace!("Looping over mice to get ELECOM mouse");
    for item in mouse_newline {
        if let Some(_) = item.find("ELECOM ELECOM TrackBall Mouse") {
            let start_num = item.rfind("\tid=").expect("id= exists") + 4;
            let end_num = item.rfind('\t').expect("has a tab");
            debug!("Remapping buttons on id {} line: {}", item.get(start_num..end_num).expect("value"), item);

            // xinput set-button-map 9 3 2 1 4 5 6 7
            let _xinput_output = Command::new("xinput").args(
                [
                    "set-button-map",
                    item.get(start_num..end_num).expect("value"),
                    "3",
                    "2",
                    "1",
                    "4",
                    "5",
                    "6",
                    "7",
                ])
                .output()
                .expect("failed to execute process");
        }
    }
}
